-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2016 at 05:17 PM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `videoplayer`
--

-- --------------------------------------------------------

--
-- Table structure for table `clips`
--

DROP TABLE IF EXISTS `clips`;
CREATE TABLE IF NOT EXISTS `clips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `start_time` int(11) NOT NULL,
  `end_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clips`
--

INSERT INTO `clips` (`id`, `name`, `start_time`, `end_time`) VALUES
(19, 'team', 6, 26),
(10, 'clip 1', 0, 10),
(11, 'clip 2', 20, 28),
(12, 'Clip 3', 28, 35);

-- --------------------------------------------------------

--
-- Table structure for table `clip_tag`
--

DROP TABLE IF EXISTS `clip_tag`;
CREATE TABLE IF NOT EXISTS `clip_tag` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `clip_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `clip_tag_clip_id_foreign` (`clip_id`),
  KEY `clip_tag_tag_id_foreign` (`tag_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `clip_tag`
--

INSERT INTO `clip_tag` (`id`, `clip_id`, `tag_id`) VALUES
(5, 11, 5),
(4, 11, 2),
(3, 10, 4),
(13, 12, 1),
(9, 10, 3),
(11, 12, 4),
(12, 12, 5),
(14, 19, 7);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_07_07_171723_create_tags_table', 2),
('2016_07_07_172935_create_clip_tag_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('cinthyarivas28@gmail.com', 'ead373b5c5b6105428bbac24da22dfda330ee41573bb955b733a62a8f73d318a', '2016-07-07 18:03:37');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'soccer', '2016-07-08 03:36:21', '2016-07-08 03:36:21'),
(2, 'futbol', '2016-07-08 03:38:51', '2016-07-08 03:38:51'),
(3, 'beautiful', '2016-07-08 03:40:03', '2016-07-08 03:40:03'),
(4, 'best clip', '2016-07-08 03:41:41', '2016-07-08 03:41:41'),
(5, 'super tag', '2016-07-08 03:50:03', '2016-07-08 03:50:03'),
(6, 'runing', '2016-07-08 19:02:21', '2016-07-08 19:02:21'),
(7, 'player', '2016-07-08 19:02:45', '2016-07-08 19:02:45');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'cinthya', 'cinthyarivas28@gmail.com', '$2y$10$lBBUUixXanACfAm/Z6r3q.hoQsitZcGzlavCmls866xQEhmixDSsu', 'lPzbIdMQq9noQYBK1xLQ3ysg2OmMi5IrDh5W9VmpqMyebcxaPMbrVk0ymOtF', '2016-07-02 00:59:18', '2016-07-07 18:08:42');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

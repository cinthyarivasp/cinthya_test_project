<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Clip;

use App\Tag;

class ClipController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of video clips.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$clips = Clip::all();

        return view('clips.index')->with('clips', $clips);
    }

    /**
     * Show the form for creating a new clip.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
    	return view('clips.create')->withTags($tags);
    }

     /**
     * Store a newly created clip in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $this->validate($request, array('name' => 'required|max:255'));
    	// $clip = new Clip;
    	// $clip->name = $request->name;
    	// $clip->start_time = $request->start_time;
    	// $clip->end_time = $request->end_time;

        //store in the database
    	$inputs = $request->all();
    	$clip = Clip::Create($inputs);

    	$clip->save();

        //sync relationship
        $clip->tags()->sync($request->tags, false);

    	return redirect()->route('clip.index');

    }

     /**
     * Display the specified clip.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$clip = Clip::find($id);
    	return view('clips.show')->with('clip', $clip);
    }

    /**
     * Show the form for editing the specified clip.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    	$clip = Clip::find($id);

        $tags = Tag::all();
        $tags2 = array();
        foreach($tags as $tag)
        {
            $tags2[$tag->id] = $tag->name;
        }
    	return view('clips.edit', compact('clip', $clip))->withTags($tags2);
    }

    /**
     * Update the specified clip in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$clip = Clip::find($id);
    	$clip->name = $request->name;
    	$clip->start_time = $request->start_time;
    	$clip->end_time = $request->end_time;

    	$clip->save();

         //sync relationship
        if(isset($request->tags)){
            $clip->tags()->sync($request->tags);   
        }else{
            $clip->tags()->sync(array());   
        }
        
    	return redirect()->route('clip.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$clip = Clip::find($id)->delete();
    	return redirect()->route('clip.index');
    }
}

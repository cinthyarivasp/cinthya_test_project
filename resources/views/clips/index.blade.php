@extends('layouts.app')
@section('title')
    Video Player Clips
@stop

@section('content')
     <div class="container">
            <h1>Video Player</h1>

            <!-- Begin Media Fragments Example -->
            <div>
              <video id="frag1" controls preload="metadata" width="100%">
                <source src="http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"  type='video/mp4;codecs="avc1.42E01E, mp4a.40.2"' data-original="http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4">
                <source src="http://clips.vorwaerts-gmbh.de/big_buck_bunny.webm" type='video/webm;codecs="vp8, vorbis"' data-original="http://clips.vorwaerts-gmbh.de/big_buck_bunny.webm">
              </video>
              <nav>
                @foreach($clips as $clip)
                  <button class="btn btn-primary" data-start="{{$clip->start_time}}" data-end="{{$clip->end_time}}">Play {{$clip->name}}</button>
                @endforeach
              </nav>
            </div>
            <h2>Clip List</h2>
            <div>
              <label>Filter</label>
              <br/>
              <input id="filter" placeholder="Type To Filter" class="form-control">
            </div>

            <div class="row">
              <div class="col-sm-12">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Start Time</th>
                          <th>End Time</th>
                          <th></th>
                        </tr>
                      <thead>

                      <tbody id="fbody">
                        @foreach($clips as $clip)
                        {!!Form::open([
                              'method' => 'delete', 
                              'route' => ['clip.destroy', $clip->id]
                        ])!!}
                        <tr>
                          <td>{{$clip->name}}</td>
                          <td>{{$clip->start_time}}</td>
                          <td>{{$clip->end_time}}</td>
                          <td>
                            @foreach($clip->tags as $tag)
                              {{ $tag->name.', '}}
                            @endforeach
                          </td>
                          <td><a href="{{route('clip.edit', $clip->id)}}" class="btn btn-primary">Edit</a>
                            {!!Form::submit('Delete', ['class' => 'btn btn-primary'])!!}
                            {!!Form::close()!!}</td>
                        </tr>
                        @endforeach
                      <tbody>
                    </table>
              </div>              
            </div>
     </div>
@endsection

@section('scripts')
    {!! Html::script('js/script.js') !!}

@endsection




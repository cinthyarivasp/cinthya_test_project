<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function clips()
    {
    	return $this->belongsToMany('App\Clip');
    }
}

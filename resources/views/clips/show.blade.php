@extends('layouts.app')
@section('title')
    {{$clip->name}}
@stop

@section('content')
	<div class="container">
		<div class="content">
			<h2> View Video Clip</h2>
			{!!Form::open([
				'method' => 'delete', 
				'route' => ['clip.destroy', $clip->id]
			])!!}
			<p>Name: {{$clip->name}}</p>
			<p>Start Time: {{$clip->start_time}}</p>
			<p>End Time: {{$clip->end_time}}</p>
			                                                       
			<a href="{{route('clip.edit', $clip->id)}}">Edit</a> 
			{!!Form::submit('Delete')!!}
			{!!Form::close()!!}

		</div>
	</div>
@endsection
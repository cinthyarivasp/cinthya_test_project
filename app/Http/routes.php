<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Pages
Route::get('/', [
    'as' => '/', 'uses' => 'PagesController@getIndex'
]);

//Authentication Routes
Route::auth();

Route::get('/home', 'HomeController@index');

//Clips
Route::resource('clip', 'ClipController');

//Tags
Route::resource('tag', 'TagController');


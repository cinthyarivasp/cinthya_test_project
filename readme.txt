===========================================================================
DESCRIPTION:
 
Project code showing how to: Display an HTML5 video player that utilizes media fragments, display list of clips to be played in the video player, create a new video clip specifying start time, end time, and name, ability to play clips in the video player,
Clip data is saved to a database, ability to delete and edit clips from the list, A registration/ login mechanism such that video clips are loaded for your account and add create ‘tags’ to clips so that they can be filtered by the tag name.
 
===========================================================================
CONFIGURATION
Import Database: database file is located in the project folder with the name videoplayer.
Make sure database name and user name in .env file match the database name you created.
DB_HOST=localhost
DB_DATABASE=videoplayer
DB_USERNAME=root
DB_PASSWORD=
 
===========================================================================
RUN APPLICATION

1. open cmd, then go to path to where they clone the project
2. Since i am using laravel, need to install dependencies running the command:
    composer install
3. git does not push .env file because it has sensitive data so you will need to create a .env file. Rename .env.example file to env.
4. run command:
    php artisan key:generate
5. run the following command:
    php artisan serve
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clip extends Model
{
   protected $fillable = [
   		'name',
   		'start_time',
   		'end_time'
   ];

   public $timestamps = false;
   protected $table = 'clips';

   public function tags()
   {
   		return $this->belongsToMany('App\Tag');
   }
}

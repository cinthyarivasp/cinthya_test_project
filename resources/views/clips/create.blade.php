@extends('layouts.app')
@section('title')
    Create
@stop
@section('stylesheets')
    {!! Html::style('css/select2.min.css') !!}
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add a New Clip</div>
                <div class="panel-body">
                     {!! Form::open(['route' => 'clip.store', 'class' => 'form-horizontal']) !!}

                        <div class="form-group">
                             {!! Form::label('name', 'Name', ['class' => 'col-md-4 control-label']) !!}

                            <div class="col-md-6">
                                 {!! Form::text('name', null, ['class' => 'form-control','placeholder' => "Enter name"]) !!}

                            </div>
                        </div>

                        <div class="form-group">
                            
                            {!! Form::label('start_time', 'Start Time', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                 {!! Form::text('start_time', null, ['class' => 'form-control', 'placeholder' => "Enter a start time"]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('end_time', 'End Time', ['class' => 'col-md-4 control-label']) !!}

                            <div class="col-md-6">
                                {!! Form::text('end_time', null, ['class' => 'form-control', 'placeholder' => "Enter a end time"]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('tags', 'Tags', ['class' => 'col-md-4 control-label']) !!}

                            <div class="col-md-6">
                               <select class="form-control select2-multi" name="tags[]" multiple="multiple">
                                  @foreach($tags as $tag)
                                  <option value='{{ $tag->id}}'> {{ $tag->name}} </option>
                                  @endforeach
                               </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                {!! Form::submit('Add a Clip', ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    {!! Html::script('js/select2.min.js') !!}

    <script type="text/javascript">
        $(".select2-multi").select2();
    </script>
@endsection


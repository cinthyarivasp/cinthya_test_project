@extends('layouts.app')
@section('title')
    Tag
@stop

@section('content')
  <div class="container">
    <div class="row">
        <div class="col-sm-8">
          <h2>Tags</h2>
          <table class="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
              </tr>
            <thead>

            <tbody>
              @foreach($tags as $tag)
              <tr>
                <th scope="row">{{$tag->id}}</th>
                <td>{{$tag->name}}</td>
              </tr>
              @endforeach
            <tbody>
          </table>
        </div>
        
        <div class="col-sm-4">
          <div class="panel panel-default">
                <div class="panel-heading">Add a New Tag</div>
                <div class="panel-body">
                     {!! Form::open(['route' => 'tag.store', 'class' => 'form-horizontal']) !!}
                             {!! Form::label('name', 'Name', ['class' => 'col-md-4 control-label']) !!}
                             {!! Form::text('name', null, ['class' => 'form-control','placeholder' => "Enter name"]) !!}
                             <br/>
                             {!! Form::submit('Add new tag', ['class' => 'btn btn-primary']) !!}                    
                      
                    {!! Form::close() !!}
                </div>
          </div>
        </div>
    </div>
  </div>
@endsection



